import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';
import Select from '@material-ui/core/Select';
import SuccessScreen from '../success_screen/SuccessScreen';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import styles from '../../styles/AddCardStyles';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
// const classes = useStyles();

// const inputLabel = React.useRef(null);
// const [labelWidth, setLabelWidth] = React.useState(0);
// React.useEffect(() => {
//   setLabelWidth(inputLabel.current.offsetWidth);
// }, []);

class DiagnoseHeartProbForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.location.search.replace('?data=', ''),
      age: '',
      gender: '',
      cp: '',
      trestbps: '',
      fbs: '',
      exang: '',
      oldpeak: '',
      slope: '',
      thal: ''
    };
    this.handleChangeAge = this.handleChangeAge.bind(this);
    this.handleChangeGender = this.handleChangeGender.bind(this);
    this.handleChangeCp = this.handleChangeCp.bind(this);
    this.handleChangeTrestbps = this.handleChangeTrestbps.bind(this);
    this.handleChangeFbs = this.handleChangeFbs.bind(this);
    this.handleChangeExang = this.handleChangeExang.bind(this);
    this.handleChangeOldpeak = this.handleChangeOldpeak.bind(this);
    this.handleChangeSlope = this.handleChangeSlope.bind(this);
    this.handleChangeThal = this.handleChangeThal.bind(this);
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleChangeAge(event) {
    this.setState({
      age: event.target.value
    });
  }

  handleChangeGender(event) {
    this.setState({
      gender: event.target.value
    })
  };

  handleChangeCp(event) {
    this.setState({
      cp: event.target.value
    })
  };

  handleChangeTrestbps(event) {
    this.setState({
      trestbps: event.target.value
    })
  };

  handleChangeFbs(event) {
    this.setState({
      fbs: event.target.value
    })
  };

  handleChangeExang(event) {
    this.setState({
      exang: event.target.value
    })
  };

  handleChangeOldpeak(event) {
    this.setState({
      oldpeak: event.target.value
    })
  };

  handleChangeSlope(event) {
    this.setState({
      slope: event.target.value
    })
  };

  handleChangeThal(event) {
    this.setState({
      thal: event.target.value
    })
  };

  componentDidMount() {
    axios.get(api.show_all_courses)
      .then(response => {
        this.onHide();
        var lc = [];
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        var res_data = response.data.data;
        if (response.data.msg_code == '001') {
          for (var i = 0; i < res_data.length; i++) {
            // this.state.listCourses.push(res_data[i].NAME)
            lc.push(res_data[i].NAME)
          }
          this.setState({ listCourses: lc })
          //  console.log(this.state.listCourses)
        }
        else {

        }
      }).catch(function (error) {
        console.log(error);
      });
  }

  handleClickButton() {
    this.onShow();
    if (
      this.state.age === '' ||
      this.state.gender === '') {
      alert("Bạn cần điền tuổi và giới tính");
      this.onHide();
    }
    else {
      let dd = require('../../utils/crypt/decrypt_data'),
        psid = dd.decrypt_data(this.state.user_data),
        data = {
          AGE: this.state.age,
          GENDER: this.state.gender,
          CP: this.state.cp,
          TRESTBPS: this.state.trestbps,
          FBS: this.state.fbs,
          EXANG: this.state.exang,
          OLDPEAK: this.state.oldpeak,
          SLOPE: this.state.slope,
          THAL: this.state.thal,
          PSID: psid
        },
        hreq = require('../../utils/handle_request/_request');
      console.log(data)

      axios.post(api.diagnose_heart_prob, data)
        .then(response => {
          this.onHide();
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          console.log(response.data)
          if (response.data.msg_code == '001') {
            return ReactDOM.render(<SuccessScreen status={response.data.data} />, document.getElementById('root'));
          }
          else {
            alert("Chẩn đoán không thành công!");
          }
        }).catch(function (error) {
          console.log(error);
        });
    }
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="NINO" src="https://benhtimmach.info.vn/wp-content/uploads/2015/04/Heart-Disease-1.jpg" style={{ width: 120, height: 120 }} />
            </Grid>
            <br />
            <Typography color="textSecondary">
              Dữ liệu chẩn đoán
          </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Giới tính</InputLabel>
              <Select style={{ width: 100 }}
                value={this.state.gender}
                onChange={this.handleChangeGender}
              >
                <MenuItem value={"male"}>Nam</MenuItem>
                <MenuItem value={"female"}>Nữ</MenuItem>
              </Select>
            </FormControl>
            <TextField
              type="number"
              id="outlined-name"
              label="Tuổi"
              className={classes.textField}
              value={this.state.age}
              onChange={this.handleChangeAge}
              variant="outlined"
            />
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Loại đau ngực</InputLabel>
              <Select style={{ width: 342 }}
                value={this.state.cp}
                onChange={this.handleChangeCp}
              >
                <MenuItem value={"1"}>Cơn đau thắt điển hình</MenuItem>
                <MenuItem value={"2"}>Cơn đau thắt không điển hình</MenuItem>
                <MenuItem value={"3"}>Đau không đau thắt</MenuItem>
                <MenuItem value={"4"}>Triệu chứng</MenuItem>
              </Select>
            </FormControl>
            <br></br>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Áp lực Trestbps</InputLabel>
              <Select style={{ width: 342 }}
                value={this.state.trestbps}
                onChange={this.handleChangeTrestbps}
              >
                <MenuItem value={"0"}>Không</MenuItem>
                <MenuItem value={"1"}>Có</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Vận động gây đau ngực</InputLabel>
              <Select style={{ width: 342 }}
                value={this.state.exang}
                onChange={this.handleChangeExang}
              >
                <MenuItem value={"0"}>Không</MenuItem>
                <MenuItem value={"1"}>Có</MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Slope</InputLabel>
              <Select style={{ width: 342 }}
                value={this.state.slope}
                onChange={this.handleChangeSlope}
              >
                <MenuItem value={"1"}>Slope đang tăng</MenuItem>
                <MenuItem value={"2"}>Phình ra</MenuItem>
                <MenuItem value={"3"}>Slope đang giảm</MenuItem>
              </Select>
            </FormControl>
            <br></br>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Thalium</InputLabel>
              <Select style={{ width: 342 }}
                value={this.state.slope}
                onChange={this.handleChangeSlope}
              >
                <MenuItem value={"3"}>Bình thường</MenuItem>
                <MenuItem value={"6"}>Khiếm khuyết cố định</MenuItem>
                <MenuItem value={"7"}>Khiếm khuyết hồi</MenuItem>
              </Select>
            </FormControl>
            <TextField
              type="number"
              id="outlined-name"
              label="Lượng đường trong máu"
              className={classes.textField}
              value={this.state.fbs}
              onChange={this.handleChangeFbs}
              variant="outlined"
            />
            <TextField
              type="number"
              id="outlined-name"
              label="Oldpeak"
              className={classes.textField}
              value={this.state.oldpeak}
              onChange={this.handleChangeOldpeak}
              variant="outlined"
            />
          </div>
          <Divider variant="middle" />
          <div className={classes.footer}>
            <Button
              style={{
                background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
                marginBottom: '2%',
              }}
              size="large"
              variant="contained"
              color="primary"
              fullWidth
              onClick={() => this.handleClickButton()}
            >
              Kiểm tra
          </Button>
          </div>
        </div>
      );
    }
  }
}

DiagnoseHeartProbForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DiagnoseHeartProbForm);