import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';

import PINVerify from './PINVerify';

import styles from '../../styles/AddCardStyles';

class TransferCardToCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: '',
      card_number: '',
      card_holder: '',
      amount: ''
    };
    this.handleChangeCardNumber = this.handleChangeCardNumber.bind(this);
    this.handleChangeCardHolder = this.handleChangeCardHolder.bind(this);
    this.handleChangeAmount = this.handleChangeAmount.bind(this);
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleChangeCardNumber(event) {
    var cn = event.target.value
    if (cn.toString().length == 16) {
      this.showCardHolder(cn)
    }
    if (cn.toString().length <= 16) {
      this.setState({
        card_number: cn
      });
    }

  }
  handleChangeCardHolder(event) {
    this.setState({
      card_holder: event.target.value
    });
  }
  handleChangeAmount(event) {
    this.setState({
      amount: event.target.value
    });
  }

  componentWillMount() {
    if (this.props.user_data) {
      this.setState({ user_data: this.props.user_data, card_number: this.props.card_number })
      this.showCardHolder(this.props.card_number)
    } else {
      this.setState({ user_data: this.props.location.search.replace('?data=', '') })
    }
  }

  showCardHolder(card_number) {
    let data = {
      card_number: card_number
    }
    console.log(data)
    axios.post(api.show_card_holder, data)
      .then(response => {
        this.onHide();
        let hres = require('../../utils/handle_response/handle_response');
        // var res_data = response.data;
        if (response.data.msg_code == '200') {
          this.setState({ card_holder: response.data.data.CARD_HOLDER })
        }
        // else {
        //   alert("Có lỗi đã xảy ra!");
        // }
      }).catch(function (error) {
        console.log(error);
      });
  }

  handleClickButton() {
    // return ReactDOM.render(<OTPInput user_data={this.state.user_data} />, document.getElementById('root'));
    this.onShow();
    if (this.state.card_number === '' || this.state.card_holder === '' || this.state.amount === '') {
      alert("Quý khách cần điền đầy đủ thông tin!");
      this.onHide();
    }
    else {
      return ReactDOM.render(
        <PINVerify
          user_data={this.state.user_data}
          card_number={this.state.card_number}
          amount={this.state.amount}
        />,
        document.getElementById('root')
      );
    }
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
            </Grid>
            {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
            <br />
            <Typography color="textSecondary">
              Xin mời thông tin.
          </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <TextField
              type="number"
              id="outlined-name"
              label="Số thẻ thụ hưởng"
              className={classes.textField}
              value={this.state.card_number}
              onChange={this.handleChangeCardNumber}
              variant="outlined"
            />
            <TextField
              // type="number"
              id="outlined-name"
              label="Tên thẻ thụ hưởng"
              className={classes.textField}
              value={this.state.card_holder}
              onChange={this.handleChangeCardHolder}
              variant="outlined"
            />
            <TextField
              type="number"
              id="outlined-name"
              label="Số tiền"
              className={classes.textField}
              value={this.state.amount}
              onChange={this.handleChangeAmount}
              variant="outlined"
            />
          </div>
          <Divider variant="middle" />
          <div className={classes.footer}>
            <Button
              style={{
                background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
                marginBottom: '2%',
              }}
              size="large"
              variant="contained"
              color="primary"
              fullWidth
              onClick={() => this.handleClickButton()}
            >
              Tiếp tục
          </Button>
          </div>
        </div>
      );
    }
  }

}

TransferCardToCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TransferCardToCard);