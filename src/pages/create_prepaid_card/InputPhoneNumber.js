import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';

import OTPInput from './OTPInput';

import styles from '../../styles/AddCardStyles';

class InputPhoneNumber extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.location.search.replace('?data=', ''),
      phone_number: '',
    };
    this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleChangePhoneNumber(event) {
    this.setState({
      phone_number: event.target.value
    });
  }

  componentDidMount() {

  }

  handleClickButton() {
    // return ReactDOM.render(<OTPInput user_data={this.state.user_data} />, document.getElementById('root'));
    this.onShow();
    if (this.state.phone_number === '') {
      alert("Quý khách cần điền đầy đủ thông tin!");
      this.onHide();
    }
    else {
      let dd = require('../../utils/crypt/decrypt_data'),
        psid = dd.decrypt_data(this.state.user_data),
        data = {
          PSID: psid,
          phone_number: this.state.phone_number
        },
        hreq = require('../../utils/handle_request/_request');
      console.log(data);
      axios.post(api.register_prepaid_card_phone_number, data)
        .then(response => {
          this.onHide();
          let hres = require('../../utils/handle_response/handle_response');
          // var res_data = response.data;
          if (response.data.msg_code == '200') {
            return ReactDOM.render(<OTPInput user_data={this.state.user_data} phone_number={this.state.phone_number} />, document.getElementById('root'));
          }
          else {
            alert("Có lỗi đã xảy ra!");
          }
        }).catch(function (error) {
          console.log(error);
        });
    }
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
            </Grid>
            {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
            <br />
            <Typography color="textSecondary">
              Xin mời nhập số điện thoại.
          </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <TextField
              type="number"
              id="outlined-name"
              label="Số điện thoại"
              className={classes.textField}
              value={this.state.phone_number}
              onChange={this.handleChangePhoneNumber}
              variant="outlined"
            />
          </div>
          <Divider variant="middle" />
          <div className={classes.footer}>
            <Button
              style={{
                background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
                marginBottom: '2%',
              }}
              size="large"
              variant="contained"
              color="primary"
              fullWidth
              onClick={() => this.handleClickButton()}
            >
              Tiếp tục
          </Button>
          </div>
        </div>
      );
    }
  }

}

InputPhoneNumber.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InputPhoneNumber);