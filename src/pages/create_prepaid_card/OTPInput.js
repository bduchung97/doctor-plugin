import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import ReactCodeInput from 'react-code-input';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import TextField from '@material-ui/core/TextField';

import api from '../../api/api_config.js';

import InfoInput from './InfoInput';

import styles from '../../styles/OTPInputStyles';

class OTPInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      otp: '',
      otp_input: 0,
      user_data: this.props.user_data,
      phone_number: this.props.phone_number
    };
    this.handleOnChangeOTP = this.handleOnChangeOTP.bind(this);
    console.log(this.props.user_data)
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleClickButton() {
    // return ReactDOM.render(<InfoInput user_data={this.state.user_data} />, document.getElementById('root'));
    this.onShow();
    if (this.state.otp == null) {
      alert("Bạn cần nhập mã xác thực!");
    }
    else {
      console.log(this.state.otp);
      let dd = require('../../utils/crypt/decrypt_data'),
        psid = dd.decrypt_data(this.state.user_data),
        data = {
          PSID: psid,
          OTP: this.state.otp,
        };
      console.log(data);
      axios.post(api.register_prepaid_card_verify_otp,
        data
      ).then(response => {
        this.onHide();
        var data = response.data;
        console.log(data);
        if (response.data.msg_code == '200') {
          return ReactDOM.render(<InfoInput user_data={this.state.user_data} phone_number={this.state.phone_number} />, document.getElementById('root'));
        }
        else {
          alert("Xác thực số điện thoại không thành công!");
        }
      }).catch(function (error) {
        console.log(error);
      });
    }
  }

  handleOnChangeOTP(event) {
    this.setState({
      otp: event.target.value
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Loading
          show={this.state.loading}
          color="blue"
          showSpinner={false}
        />
        <div className={classes.header}>
          <Grid container justify="center" alignItems="center">
            <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" className={classes.bigAvatar} />
          </Grid>
          <Grid container alignItems="center">
            <Typography gutterBottom variant="h4">
              MSB
            </Typography>
          </Grid>
          <Typography color="textSecondary">
            Mời quý khách nhập mã OTP được gửi qua SMS.
          </Typography>
        </div>
        <Divider variant="middle" />
        <div className={classes.body}>
          <TextField
            type="number"
            id="outlined-name"
            label="Mã OTP"
            className={classes.textField}
            value={this.state.otp}
            onChange={this.handleOnChangeOTP}
            variant="outlined"
            style={{ width: '100%' }}
          />
        </div>
        <Divider variant="middle" />
        <div className={classes.footer}>
          <Button
            style={{
              background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
              borderRadius: 3,
              border: 0,
              color: 'white',
              height: 48,
              padding: '0 30px',
              boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
              marginBottom: '2%',
            }}
            size="large"
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => this.handleClickButton()}
          >
            Xác nhận
          </Button>
        </div>
      </div>
    );
  }

}

OTPInput.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(OTPInput);