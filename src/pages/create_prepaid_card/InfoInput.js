import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';
import ReactCodeInput from 'react-code-input';

import SuccessScreen from '../success_screen/SuccessScreen';

import styles from '../../styles/AddCardStyles';

class InfoInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.user_data,
      card_holder: '',
      pin: '',
      pin_input: 0,
      pin_2: '',
      pin_input_2: 0,
      phone_number: this.props.phone_number
    };
    this.handleChangeCardHolder = this.handleChangeCardHolder.bind(this);
    this.handleOnChangePIN = this.handleOnChangePIN.bind(this);
    this.handleOnChangePIN2 = this.handleOnChangePIN2.bind(this);
  }

  handleOnChangePIN(event) {
    this.setState({
      pin: event.target.value
    });
  }
  handleOnChangePIN2(event) {
    this.setState({
      pin_2: event.target.value
    });
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleChangeCardHolder(event) {
    this.setState({
      card_holder: event.target.value.toUpperCase()
    });
  }

  handleClickButton() {
    // return ReactDOM.render(<SuccessScreen />, document.getElementById('root'));
    this.onShow();
    if (this.state.card_holder === '' || this.state.pin === '' || this.state.pin_2 === '') {
      alert("Quý khách cần điền đầy đủ thông tin!");
      this.onHide();
    } else if (this.state.pin != this.state.pin_2) {
      alert("Mã xác nhận mã PIN không đúng!");
      this.onHide();
    }
    else {
      let dd = require('../../utils/crypt/decrypt_data'),
        psid = dd.decrypt_data(this.state.user_data),
        data = {
          fullname: this.state.card_holder,
          PIN: this.state.pin,
          phone_number: this.state.phone_number,
          PSID: psid
        },
        hreq = require('../../utils/handle_request/_request');
      axios.post(api.register_prepaid_card_info, data)
        .then(response => {
          this.onHide();
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          // var res_data = response.data;
          if (response.data.msg_code == '200') {
            return ReactDOM.render(<SuccessScreen />, document.getElementById('root'));
          }
          else {
            alert("Đăng nhập không thành công!");
          }
        }).catch(function (error) {
          console.log(error);
        });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Loading
          show={this.state.loading}
          color="blue"
          showSpinner={false}
        />
        <div className={classes.header}>
          <Grid container justify="center" alignItems="center">
            <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
          </Grid>
          {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
          <br />
          <Typography color="textSecondary">
            Xin mời nhập thông tin.
          </Typography>
        </div>
        <Divider variant="middle" />
        <div className={classes.body}>
          <TextField
            id="outlined-name"
            label="Họ tên chủ thẻ"
            className={classes.textField}
            value={this.state.card_holder}
            onChange={this.handleChangeCardHolder}
            variant="outlined"
          />
          <TextField
            type="password"
            id="outlined-name"
            label="Mật khẩu"
            className={classes.textField}
            value={this.state.pin}
            onChange={this.handleOnChangePIN}
            variant="outlined"
            style={{ width: '100%' }}
          />
          <TextField
            type="password"
            id="outlined-name"
            label="Xác nhận mật khẩu"
            className={classes.textField}
            value={this.state.pin_2}
            onChange={this.handleOnChangePIN2}
            variant="outlined"
            style={{ width: '100%' }}
          />
        </div>
        <Divider variant="middle" />
        <div className={classes.footer}>
          <Button
            style={{
              background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
              borderRadius: 3,
              border: 0,
              color: 'white',
              height: 48,
              padding: '0 30px',
              boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
              marginBottom: '2%',
            }}
            size="large"
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => this.handleClickButton()}
          >
            Kích hoạt thẻ
          </Button>
        </div>
      </div>
    );
  }
}

InfoInput.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InfoInput);