import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';

import styles from '../../styles/AddCardStyles';

class TransactionHistory extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.user_data,
      transaction_history: []
    };
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  componentDidMount() {
    this.onShow();
    let dd = require('../../utils/crypt/decrypt_data'),
      data = {
        PSID: dd.decrypt_data(this.state.user_data),
      };
    console.log(data);
    axios.post(api.transaction_history,
      data
    ).then(response => {
      this.onHide();
      // var data = response.data;
      // console.log(data);
      if (response.data.msg_code == '200') {
        var array = response.data.data;
        array.map((e, i) => {
          if (e.SOURCE_NUMBER) {
            // e.SOURCE_NUMBER = 'Từ: ' + e.SOURCE_NUMBER;
            Object.assign(e, { card_number: 'Từ: ' + e.SOURCE_NUMBER, img: 'https://i.imgur.com/ObnZ7zV.png' });
          }

          if (e.TARGET_NUMBER) {
            // e.TARGET_NUMBER = 'Tới: ' + e.TARGET_NUMBER;
            Object.assign(e, { card_number: 'Tới: ' + e.TARGET_NUMBER, img: 'https://i.imgur.com/TKyeIgv.png' });
          }
          e.CREATED_AT = e.CREATED_AT.replace('T', ' ').replace('Z', '').slice(0, 19)
        })
        this.setState({
          transaction_history: array
        })
        console.log(array)
      }
      else {
        alert("Đã có lỗi xảy ra!");
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
            </Grid>
            {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
            <br />
            <Typography color="textSecondary">
              Lịch sử giao dịch.
            </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>

            <List>
              {this.state.transaction_history.map((e, i) => {
                return (
                  <ListItem key={i}>
                    <ListItemAvatar>
                      <Avatar src={e.img} />
                    </ListItemAvatar>
                    <ListItemText style={{ marginLeft: '5%' }} primary={e.card_number} secondary={e.CREATED_AT} />
                    <ListItemText style={{ marginLeft: '5%', fontWeight: 'bold' }} >
                      {e.AMOUNT}
                    </ListItemText>
                  </ListItem>
                )
              })}
            </List>
          </div>
        </div>
      );
    }
  }

}

TransactionHistory.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TransactionHistory);