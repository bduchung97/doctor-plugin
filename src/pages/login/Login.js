import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';

import SuccessScreen from '../success_screen/SuccessScreen';

import styles from '../../styles/AddCardStyles';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.location.search.replace('?data=', ''),
      email: '',
      password: '',
    };
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleChangeEmail(event) {
    this.setState({
      email: event.target.value
    });
  }

  handleChangePassword(event) {
    this.setState({
      password: event.target.value
    });
  }

  handleClickButton() {
    this.onShow();
    // if (this.state.email === '' || this.state.password === '') {
    //   alert("Quý khách cần điền đầy đủ thông tin!");
    //   this.onHide();
    // }
    // else {
    //   let dd = require('../../utils/crypt/decrypt_data'),
    //     psid = dd.decrypt_data(this.state.user_data),
    //     data = {
    //       username: this.state.email,
    //       password: this.state.password,
    //       PSID: psid
    //     },
    //     hreq = require('../../utils/handle_request/_request');
    //   axios.post(api.login, hreq._request(data))
    //     .then(response => {
    //       this.onHide();
    //       let hres = require('../../utils/handle_response/handle_response');
    //       var res_data = hres.handle_response(response.data);
    //       // var res_data = response.data;
    //       if (res_data.msg_code == '001') {
    //         return ReactDOM.render(<SuccessScreen />, document.getElementById('root'));
    //       }
    //       else {
    //         alert("Đăng nhập không thành công!");
    //       }
    //     }).catch(function (error) {
    //       console.log(error);
    //     });
    // }
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Loading
          show={this.state.loading}
          color="blue"
          showSpinner={false}
        />
        <div className={classes.header}>
          <Grid container justify="center" alignItems="center">
            <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
          </Grid>
          {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
          <br />
          <Typography color="textSecondary">
            Xin mời đăng nhập.
          </Typography>
        </div>
        <Divider variant="middle" />
        <div className={classes.body}>
          <TextField
            id="outlined-name"
            label="Email"
            className={classes.textField}
            value={this.state.email}
            onChange={this.handleChangeEmail}
            variant="outlined"
          />
          <TextField
            id="outlined-name"
            label="Mật khẩu"
            className={classes.textField}
            value={this.state.password}
            onChange={this.handleChangePassword}
            variant="outlined"
            type="password"
          />
        </div>
        <Divider variant="middle" />
        <div className={classes.footer}>
          <Button
            style={{
              background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
              borderRadius: 3,
              border: 0,
              color: 'white',
              height: 48,
              padding: '0 30px',
              boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
              marginBottom: '2%',
            }}
            size="large"
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => this.handleClickButton()}
          >
            Đăng nhập
          </Button>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);