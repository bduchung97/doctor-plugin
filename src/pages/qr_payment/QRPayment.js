import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import QrReader from 'react-qr-reader';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';

import TransferCardToCard from '../transfer_card_to_card/TransferCardToCard';

import styles from '../../styles/AddCardStyles';

class QRPayment extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.location.search.replace('?data=', ''),
      qr_data: ''
    };
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleScan = data => {
    if (data) {
      console.log(data);
      try {
        var a = JSON.parse(data)
        if (a.createdBy == 'MSB') {
          console.log(a.data);
          return ReactDOM.render(
            <TransferCardToCard
              user_data={this.state.user_data}
              card_number={a.data}
            />,
            document.getElementById('root')
          );
        }
      } catch (error) {
        console.log(error)
      }
      // this.setState({
      //   qr_data: JSON.parse(data)
      // })
    }
  }
  handleError = err => {
    console.error(err)
  }

  componentDidMount() {

  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
            </Grid>
            {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
            <br />
            <Typography color="textSecondary">
              Quét mã QR.
            </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <Grid container justify="center" alignItems="center">
              <QrReader
                facingMode={'environment'}
                delay={300}
                onError={this.handleError}
                onScan={this.handleScan}
                style={{ width: '100%' }}
              />
            </Grid>
          </div>
        </div>
      );
    }
  }

}

QRPayment.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(QRPayment);