import React from 'react';
import {
	Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';
import ReactCodeInput from 'react-code-input';
import Select from '@material-ui/core/Select';
import SuccessScreen from '../success_screen/SuccessScreen';
import ThongBaoThanhCong from '../thong_bao_thanhcong/ThongBaoThanhCong';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import styles from '../../styles/AddCardStyles';
import Calendar from 'react-calendar';

const useStyles = makeStyles(theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120,
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
}));
// const classes = useStyles();

// const inputLabel = React.useRef(null);
// const [labelWidth, setLabelWidth] = React.useState(0);
// React.useEffect(() => {
//   setLabelWidth(inputLabel.current.offsetWidth);
// }, []);

class CreateAppointmentBachMai extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: false,
			user_data: this.props.location.search.replace('?data=', ''),
			fullname: '',
			phone_number: '',
			dob: '',
			gender: '',
			email: '',
			shift: '',
			day_appointment: '',
			address: '',
		};
		this.handleChangeFullName = this.handleChangeFullName.bind(this);
		this.handleDateChange = this.handleDateChange.bind(this);
		this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
		this.handleChangeGender = this.handleChangeGender.bind(this);
		this.handleChangeEmail = this.handleChangeEmail.bind(this);
		this.handleChangeShift = this.handleChangeShift.bind(this);
		this.handleChangeDOB = this.handleChangeDOB.bind(this);
		this.handleChangeDayAppointment = this.handleChangeDayAppointment.bind(this);
		this.handleChangeAddress = this.handleChangeAddress.bind(this);
	}

	onShow = () => {
		this.setState({ loading: true })
	}

	onHide = () => {
		this.setState({ loading: false })
	}

	handleChangeFullName(event) {
		this.setState({
			fullname: event.target.value
		});
	}

	handleDateChange(event) {
		this.setState({ dob: event.target.value })
	}

	handleChangePhoneNumber(event) {
		this.setState({
			phone_number: event.target.value
		});
	}

	handleChangeGender(event) {
		this.setState({
			gender: event.target.value
		})
	}

	handleChangeShift(event) {
		this.setState({
			shift: event.target.value
		})
	}

	handleChangeDayAppointment(event) {
		this.setState({
			day_appointment: event.target.value
		})
	}

	handleChangeEmail(event) {
		this.setState({
			email: event.target.value
		})
	}

	handleChangeAddress(event) {
		this.setState({
			address: event.target.value
		})
	}

	handleChangeDOB = date => {
		this.setState({
			dob: date
		})
	}

	handleClickButton() {
		this.onShow();
		if (this.state.fullname === '' ||
			this.state.email === '' ||
			this.state.phone_number === '' ||
			this.state.shift === '' ||
			this.state.day_appointment === '' ||
			this.state.dob === '' ||
			this.state.address === '' ||
			this.state.gender === '') {
			alert("Bạn cần điền đầy đủ thông tin!");
			this.onHide();
		}
		else {
			let dd = require('../../utils/crypt/decrypt_data'),
				psid = dd.decrypt_data(this.state.user_data),
				data = {
					FULLNAME: this.state.fullname,
					DOB: this.state.dob,
					GENDER: this.state.gender,
					EMAIL: this.state.email,
					PHONE_NUMBER: this.state.phone_number,
					SHIFT: this.state.shift,
					DAY_APPOINTMENT: this.state.day_appointment,
					ADDRESS: this.state.address,
					PSID: psid
				},
				hreq = require('../../utils/handle_request/_request');
			axios.post(api.create_appointment_bachmai, data)
				.then(response => {
					this.onHide();
					let hres = require('../../utils/handle_response/handle_response');
					var res_data = hres.handle_response(response.data);
					console.log(response.data)
					// var res_data = response.data;
					if (response.data.msg_code == '001') {
						return ReactDOM.render(<ThongBaoThanhCong />, document.getElementById('root'));
					}
					else {
						alert("Đăng kí không thành công!");
					}
				}).catch(function (error) {
					console.log(error);
				});
		}
	}

	render() {
		let dd = require('../../utils/crypt/decrypt_data'),
			psid = dd.decrypt_data(this.state.user_data)
		if (!psid) {
			return <Redirect to={{ pathname: "/" }} />;
		} else {
			const { classes } = this.props;
			return (
				<div className={classes.root}>
					<Loading
						show={this.state.loading}
						color="blue"
						showSpinner={false}
					/>
					<div className={classes.header}>
						<Grid container justify="center" alignItems="center">
							<Avatar alt="NINO" src="https://chosithuoc.com/uploads/danhmuc/benh-vien-bach-mai-img.png" style={{ width: 120, height: 120 }} />
						</Grid>
						<br />
						<Typography color="textSecondary">
							Bệnh viện Bạch Mai - Hà Nội
          </Typography>
					</div>
					<Divider variant="middle" />
					<div className={classes.body}>
						<TextField
							id="outlined-name"
							label="Họ tên đầy đủ"
							className={classes.textField}
							value={this.state.fullname}
							onChange={this.handleChangeFullName}
							variant="outlined"
						/>
						<TextField
							id="outlined-name"
							label="Ngày sinh (ngày/tháng/năm)"
							className={classes.textField}
							value={this.state.dob}
							onChange={this.handleDateChange}
							variant="outlined"
						/>
						<br />
						<FormControl className={classes.formControl}>
							<InputLabel htmlFor="age-simple">Giới tính</InputLabel>
							<Select style={{ width: 200 }}
								value={this.state.gender}
								onChange={this.handleChangeGender}
							>
								<MenuItem value={"Nam"}>Nam</MenuItem>
								<MenuItem value={"Nữ"}>Nữ</MenuItem>
							</Select>
						</FormControl>
						<TextField
							type="number"
							id="outlined-name"
							label="Số điện thoại"
							className={classes.textField}
							value={this.state.phone_number}
							onChange={this.handleChangePhoneNumber}
							variant="outlined"
						/>
						<TextField
							type="email"
							id="outlined-name"
							label="Email"
							className={classes.textField}
							value={this.state.email}
							onChange={this.handleChangeEmail}
							variant="outlined"
						/>
						<TextField
							id="outlined-name"
							label="Ngày khám (ngày/tháng/năm)"
							className={classes.textField}
							value={this.state.day_appointment}
							onChange={this.handleChangeDayAppointment}
							variant="outlined"
						/>
						<br></br>
						<FormControl className={classes.formControl}>
							<InputLabel htmlFor="age-simple">Ca khám bệnh </InputLabel>
							<Select style={{ width: 200 }}
								value={this.state.shift}
								onChange={this.handleChangeShift}
							>
								<MenuItem value={"14h - 16h"}>14h - 16h</MenuItem>
								<MenuItem value={"16h - 18h"}>16h - 18h</MenuItem>
								<MenuItem value={"18h - 20h"}>18h - 20h</MenuItem>
								<MenuItem value={"20h - 22h"}>20h - 22h</MenuItem>
							</Select>
						</FormControl>
						<TextField
							type="address"
							id="outlined-name"
							label="Địa chỉ"
							className={classes.textField}
							value={this.state.address}
							onChange={this.handleChangeAddress}
							variant="outlined"
						/>
					</div>
					<Divider variant="middle" />
					<div className={classes.footer}>
						<Button
							style={{
								background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
								borderRadius: 3,
								border: 0,
								color: 'white',
								height: 48,
								padding: '0 30px',
								boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
								marginBottom: '2%',
							}}
							size="large"
							variant="contained"
							color="primary"
							fullWidth
							onClick={() => this.handleClickButton()}
						>
							Đăng kí
          </Button>
					</div>
				</div>
			);
		}
	}
}

CreateAppointmentBachMai.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateAppointmentBachMai);