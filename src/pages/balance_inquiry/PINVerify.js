import React from 'react';
import ReactDOM from 'react-dom';
import {
  Redirect,
} from "react-router-dom";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import ReactCodeInput from 'react-code-input';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import TextField from '@material-ui/core/TextField';

import api from '../../api/api_config.js';

import Balance from './Balance';

import styles from '../../styles/OTPInputStyles';

class PINVerify extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      pin: '',
      pin_input: '',
      user_data: this.props.location.search.replace('?data=', ''),
    };
    this.handleOnChangePin = this.handleOnChangePin.bind(this);
    console.log(this.props.user_data)
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleClickButton() {
    // return ReactDOM.render(<Balance user_data={this.state.user_data} />, document.getElementById('root'));
    this.onShow();
    if (this.state.pin == null) {
      alert("Bạn cần nhập mã PIN!");
    }
    else {
      console.log(this.state.pin);
      let dd = require('../../utils/crypt/decrypt_data'),
        data = {
          PSID: dd.decrypt_data(this.state.user_data),
          PIN: this.state.pin,
        };
      console.log(data);
      axios.post(api.verify_PIN,
        data
      ).then(response => {
        this.onHide();
        // var data = response.data;
        // console.log(data);
        if (response.data.msg_code == '200') {
          return ReactDOM.render(<Balance user_data={this.state.user_data} />, document.getElementById('root'));
        }
        else {
          alert("Xác thực PIN không thành công!");
        }
      }).catch(function (error) {
        console.log(error);
      });
    }
  }

  handleOnChangePin(event) {
    this.setState({
      pin: event.target.value
    });
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" className={classes.bigAvatar} />
            </Grid>
            <Grid container alignItems="center">
              <Typography gutterBottom variant="h4">
                MSB
            </Typography>
            </Grid>
            <Typography color="textSecondary">
              Mời quý khách nhập mã PIN bảo mật.
          </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <TextField
              type="password"
              id="outlined-name"
              label="Mật khẩu"
              className={classes.textField}
              value={this.state.pin}
              onChange={this.handleOnChangePin}
              variant="outlined"
              style={{ width: '100%' }}
            />
          </div>
          <Divider variant="middle" />
          <div className={classes.footer}>
            <Button
              style={{
                background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
                marginBottom: '2%',
              }}
              size="large"
              variant="contained"
              color="primary"
              fullWidth
              onClick={() => this.handleClickButton()}
            >
              Xác nhận
          </Button>
          </div>
        </div>
      );
    }
  }

}

PINVerify.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PINVerify);