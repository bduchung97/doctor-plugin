import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';

import styles from '../../styles/AddCardStyles';

class Balance extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.user_data,
      card_number: 'XXXX XXXX XXXX XXXX',
      balance: '0'
    };
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  componentDidMount() {
    this.onShow();
    let dd = require('../../utils/crypt/decrypt_data'),
      data = {
        PSID: dd.decrypt_data(this.state.user_data),
      };
    console.log(data);
    axios.post(api.balance_inquiry_prepaid_card,
      data
    ).then(response => {
      this.onHide();
      // var data = response.data;
      // console.log(data);
      if (response.data.msg_code == '200') {
        this.setState({
          balance: response.data.data.balance_amount,
          card_number: response.data.data.card_number
        })
      }
      else {
        alert("Đã có lỗi xảy ra!");
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
            </Grid>
            {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
            <br />
            <Typography color="textSecondary">
              Truy vấn số dư.
            </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <Typography gutterBottom variant="h5">
              Mã số thẻ: {this.state.card_number}
            </Typography>
            <Typography gutterBottom variant="h5">
              Số dư khả dụng: {this.state.balance} VNĐ
            </Typography>
          </div>
        </div>
      );
    }
  }

}

Balance.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Balance);