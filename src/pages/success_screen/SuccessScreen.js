import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import styles from '../../styles/SuccessScreenStyles';

function SuccessScreen(props) {
  const { classes } = props;
  return (
    <div>
      <Grid container justify="center" alignItems="center">
        <Avatar alt="QBank" src="https://png.pngtree.com/svg/20170401/nine_rich_wallet_success_icon_112949.png" className={classes.bigAvatar} />
      </Grid>
      <br />
      <Grid container justify="center" alignItems="center">
        <Typography gutterBottom variant="h5">
        Kết quả chẩn đoán của bạn là: {props.status}
        </Typography>
      </Grid>
    </div>
  );
}

SuccessScreen.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SuccessScreen);