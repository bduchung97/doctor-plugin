import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';
import Select from '@material-ui/core/Select';
import SuccessScreen from '../success_screen/SuccessScreen';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import styles from '../../styles/AddCardStyles';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
// const classes = useStyles();

// const inputLabel = React.useRef(null);
// const [labelWidth, setLabelWidth] = React.useState(0);
// React.useEffect(() => {
//   setLabelWidth(inputLabel.current.offsetWidth);
// }, []);

class RegisterClassForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.location.search.replace('?data=', ''),
      fullname: '',
      phone_number: '',
      dob: '',
      gender: '',
      email: '',
      listCourses: [],
      course: ''
    };
    this.handleChangeFullName = this.handleChangeFullName.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
    this.handleChangeGender = this.handleChangeGender.bind(this);
    this.handleChangeCourse = this.handleChangeCourse.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangeDOB = this.handleChangeDOB.bind(this);
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  handleChangeFullName(event) {
    this.setState({
      fullname: event.target.value
    });
  }

  handleDateChange(event) {
    this.setState({ dob: event.target.value })
  }

  handleChangePhoneNumber(event) {
    this.setState({
      phone_number: event.target.value
    });
  }

  handleChangeGender(event) {
    this.setState({
      gender: event.target.value
    })
  }

  handleChangeCourse(event) {
    this.setState({
      course: event.target.value
    })
  }

  handleChangeEmail(event) {
    this.setState({
      email: event.target.value
    })
  }

  handleChangeDOB = event => {
    this.setState({
      dob: event.target.value
    })
  }

  componentDidMount() {
    axios.get(api.show_all_courses)
      .then(response => {
        this.onHide();
        var lc = [];
        let hres = require('../../utils/handle_response/handle_response');
        var res_data = hres.handle_response(response.data);
        var res_data = response.data.data;
        if (response.data.msg_code == '001') {
          for (var i = 0; i<res_data.length; i++) {
            // this.state.listCourses.push(res_data[i].NAME)
            lc.push(res_data[i].NAME)
          }
          this.setState({listCourses: lc})
        //  console.log(this.state.listCourses)
        }
        else {

        }
      }).catch(function (error) {
        console.log(error);
      });
  }

  handleClickButton() {
    this.onShow();
    if (this.state.fullname === '' ||
      this.state.email === '' ||
      this.state.phone_number === '' ||
      this.state.course === '' ||
      this.state.dob === '' ||
      this.state.gender === '') {
      alert("Bạn cần điền đầy đủ thông tin!");
      this.onHide();
    }
    else {
      let dd = require('../../utils/crypt/decrypt_data'),
        psid = dd.decrypt_data(this.state.user_data),
        data = {
          FULLNAME: this.state.fullname,
          DOB: this.state.dob,
          GENDER: this.state.gender,
          EMAIL: this.state.email,
          PHONE_NUMBER: this.state.phone_number,
          CLASS: this.state.course,
          PSID: psid
        },
        hreq = require('../../utils/handle_request/_request');
        console.log(data)
      axios.post(api.register_class_form, data)
        .then(response => {
          this.onHide();
          let hres = require('../../utils/handle_response/handle_response');
          var res_data = hres.handle_response(response.data);
          console.log(response.data)
          if (response.data.msg_code == '001') {
            return ReactDOM.render(<SuccessScreen />, document.getElementById('root'));
          }
          else {
            alert("Đăng kí không thành công!");
          }
        }).catch(function (error) {
          console.log(error);
        });
    }
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="NINO" src="https://i.imgur.com/qtDlBIt.png" style={{ width: 120, height: 120 }} />
            </Grid>
            <br />
            <Typography color="textSecondary">
              Đăng kí lớp học
          </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <TextField
              id="outlined-name"
              label="Họ tên đầy đủ"
              className={classes.textField}
              value={this.state.fullname}
              onChange={this.handleChangeFullName}
              variant="outlined"
            />
             <TextField
              id="outlined-name"
              label="Ngày sinh (ngày/tháng/năm)"
              className={classes.textField}
              value={this.state.dob}
              onChange={this.handleChangeDOB}
              variant="outlined"
            />
            <br />
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Giới tính</InputLabel>
              <Select style={{ width: 100 }}
                value={this.state.gender}
                onChange={this.handleChangeGender}
              >
                <MenuItem value={"Nam"}>Nam</MenuItem>
                <MenuItem value={"Nữ"}>Nữ</MenuItem>
              </Select>
            </FormControl>
            <TextField
              type="number"
              id="outlined-name"
              label="Số điện thoại"
              className={classes.textField}
              value={this.state.phone_number}
              onChange={this.handleChangePhoneNumber}
              variant="outlined"
            />
            <TextField
              type="email"
              id="outlined-name"
              label="Email"
              className={classes.textField}
              value={this.state.email}
              onChange={this.handleChangeEmail}
              variant="outlined"
            />
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="age-simple">Khóa học</InputLabel>
              <Select style={{ width: 200 }}
                value={this.state.course}
                onChange={this.handleChangeCourse}
              >
                
                {/* <MenuItem value={0}> Danh sách khóa học</MenuItem> */}
                {this.state.listCourses.map((i) =>
                  <MenuItem value={i}>{i}</MenuItem>
                )}
              </Select>
            </FormControl>
          </div>
          <Divider variant="middle" />
          <div className={classes.footer}>
            <Button
              style={{
                background: 'linear-gradient(45deg, #da2128 30%, #f37021 90%)',
                borderRadius: 3,
                border: 0,
                color: 'white',
                height: 48,
                padding: '0 30px',
                boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
                marginBottom: '2%',
              }}
              size="large"
              variant="contained"
              color="primary"
              fullWidth
              onClick={() => this.handleClickButton()}
            >
              Đăng kí
          </Button>
          </div>
        </div>
      );
    }
  }
}

RegisterClassForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RegisterClassForm);