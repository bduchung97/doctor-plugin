import React from 'react';
import {
  Redirect,
} from "react-router-dom";
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import api from '../../api/api_config.js';

import styles from '../../styles/AddCardStyles';

class CreateQR extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      user_data: this.props.location.search.replace('?data=', ''),
      qr: ''
    };
  }

  onShow = () => {
    this.setState({ loading: true })
  }

  onHide = () => {
    this.setState({ loading: false })
  }

  componentDidMount() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data),
      data = {
        PSID: psid,
      };
    console.log(data);
    axios.post(api.create_qr_prepaid_card,
      data
    ).then(response => {
      this.onHide();
      var data = response.data;
      console.log(data);
      if (response.data.msg_code == '200') {
        this.setState({ qr: data.data })
      }
      else {
        alert("Sinh mã QR không thành công!");
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  render() {
    let dd = require('../../utils/crypt/decrypt_data'),
      psid = dd.decrypt_data(this.state.user_data)
    if (!psid) {
      return <Redirect to={{ pathname: "/" }} />;
    } else {
      const { classes } = this.props;
      return (
        <div className={classes.root}>
          <Loading
            show={this.state.loading}
            color="blue"
            showSpinner={false}
          />
          <div className={classes.header}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="MSB" src="https://i.imgur.com/8BmuC0f.png" style={{ width: 120, height: 120 }} />
            </Grid>
            {/* <Grid container alignItems="center">
            <Typography gutterBottom variant="h4" style={{ marginTop: '5%' }}>
              MSB Chatbot
            </Typography>
          </Grid> */}
            <br />
            <Typography color="textSecondary">
              Đây là mã QR của bạn.
            </Typography>
          </div>
          <Divider variant="middle" />
          <div className={classes.body}>
            <Grid container justify="center" alignItems="center">
              <img src={this.state.qr} style={{ width: '100%' }} />
            </Grid>
          </div>
        </div>
      );
    }
  }

}

CreateQR.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CreateQR);