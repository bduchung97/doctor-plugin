import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import styles from '../../styles/HomePageStyles';

function HomePage(props) {
  const { classes } = props;
  return (
    <div>
      <Grid container justify="center" alignItems="center">
        <Avatar alt="NINO" src="https://i.imgur.com/hHyseAX.png" className={classes.bigAvatar} />
      </Grid>
      <Grid container justify="center" alignItems="center">
        <Typography gutterBottom variant="h4">
          NINO
        </Typography>
      </Grid>
    </div>
  );
}

HomePage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomePage);