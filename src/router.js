import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import HomePage from './pages/home_page/HomePage';
import SuccessScreen from './pages/success_screen/SuccessScreen';
import InputPhoneNumber from './pages/create_prepaid_card/InputPhoneNumber';
import PINVerifyBalanceInquiry from './pages/balance_inquiry/PINVerify';
import TopUp from './pages/top_up/TopUp';
import CreateQR from './pages/create_qr/CreateQR';
import TransferCardToCard from './pages/transfer_card_to_card/TransferCardToCard';
import QRPayment from './pages/qr_payment/QRPayment';
import TransactionHis from './pages/transaction_history/PINVerify';
import Login from './pages/login/Login';
import ThongBaoThanhCong from './pages/thong_bao_thanhcong/ThongBaoThanhCong';
import RegisterClassForm from './pages/register_class_form/RegisterClassForm'
import RegisterTestForm from './pages/register_test_form/RegisterTestForm'
import DiagnoseForm from "./pages/diagnose_form/DiagnoseForm";
import DiagnoseHeartProbForm from "./pages/diagnose_heart_prob_form/DiagnoseHeartProbForm";
import CreateAppointmentBachMai from "./pages/create_appointment_bachmai/CreateAppointmentBachMai";

const AppRouter = () => (
  <Router>
    <div>
      <Route exact path="/" component={HomePage} />
      <Route path="/success" component={SuccessScreen} />
      <Route path="/thanh_cong" component={ThongBaoThanhCong} />
      <Route path="/create-prepaid-card" component={InputPhoneNumber} />
      <Route path="/balance-inquiry-prepaid-card" component={PINVerifyBalanceInquiry} />
      <Route path="/top-up-guide-prepaid-card" component={TopUp} />
      <Route path="/create-qr-prepaid-card" component={CreateQR} />
      <Route path="/transfer-c2c-prepaid-card" component={TransferCardToCard} />
      <Route path="/qr-payment-prepaid-card" component={QRPayment} />
      <Route path="/transaction-history-prepaid-card" component={TransactionHis} />
      <Route path="/login" component={Login} />

      <Route path="/register_class_form" component={RegisterClassForm} />
      <Route path="/register_test_form" component={RegisterTestForm} />

      <Route path="/diagnose_form" component={DiagnoseForm} />
      <Route path="/diagnose_heart_prob_form" component={DiagnoseHeartProbForm} />
      <Route path="/create_appointment_bachmai" component={CreateAppointmentBachMai} />
    </div>
  </Router>
);

export default AppRouter;
