const SERVER_URL = 'https://doctor-bot-server.appspot.com/',
  SERVER_PORT = '';
// const SERVER_URL = 'http://localhost',
//   SERVER_PORT = ':1337';

module.exports = {
  register_prepaid_card_phone_number: SERVER_URL + SERVER_PORT + '/post_register_phone_number',
  register_prepaid_card_verify_otp: SERVER_URL + SERVER_PORT + '/post_verify_otp',
  register_prepaid_card_info: SERVER_URL + SERVER_PORT + '/post_register_card_info',
  create_qr_prepaid_card: SERVER_URL + SERVER_PORT + '/post_create_qr_code',
  verify_PIN: SERVER_URL + SERVER_PORT + '/post_verify_PIN',
  balance_inquiry_prepaid_card: SERVER_URL + SERVER_PORT + '/post_balance_inquiry',
  show_card_holder: SERVER_URL + SERVER_PORT + '/post_show_card_holder',
  transfer_c2c: SERVER_URL + SERVER_PORT + '/post_transfer_card_to_card',
  transaction_history: SERVER_URL + SERVER_PORT + '/post_show_transaction_history',
  show_info_prepaid_card: SERVER_URL + SERVER_PORT + '/post_show_card_info',

  register_class_form: SERVER_URL + SERVER_PORT + '/post_register_class',
  register_test_form: SERVER_URL + SERVER_PORT + '/post_book_test_exam',
  show_all_courses: SERVER_URL + SERVER_PORT + '/get_show_all_courses',

  diagnose: SERVER_URL + SERVER_PORT + '/post_diagnose',
  diagnose_heart_prob: SERVER_URL + SERVER_PORT + '/post_kiem_tra_benh',

  create_appointment_bachmai: SERVER_URL + SERVER_PORT + '/post_create_appointment_bachmai'
}
