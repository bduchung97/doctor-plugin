let CryptoJS = require("crypto-js");

exports.decrypt_data = function decrypt_data(data) {
  if (typeof data == 'string') {
    let key = process.env.MASTER_KEY || 'doctor-bot',
      bytes = CryptoJS.AES.decrypt(data, key);
    if (parseInt(bytes.sigBytes) > 0) {
      return bytes.toString(CryptoJS.enc.Utf8);
    }
    else {
      return null;
    }
  } else {
    return null;
  }
}