exports._request = function _request(req_data, session_token) {
  let ed = require('../crypt/encrypt_data.js'),
    cs = require('../crypt/checksum.js'),
    data = {
      encrypted_data: ed.encrypt_data(JSON.stringify(req_data)),
      session_token: session_token
    }
  return {
    data: data,
    checksum: cs.checksum(JSON.stringify(data)),
  }
}
