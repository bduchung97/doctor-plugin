const styles = theme => ({
  // root: {
  //   width: '100%',
  //   // maxWidth: 30,
  //   backgroundColor: theme.palette.background.paper,
  // },
  header: {
    margin: `${theme.spacing.unit * 3}px ${theme.spacing.unit * 2}px`,
  },
  body: {
    margin: theme.spacing.unit * 2,
    alignItems: 'center',
  },
  footer: {
    margin: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    alignItems: 'center'
  },
  bigAvatar: {
    width: 100,
    height: 100,
  },
  smallAvatar: {
    width: 50,
    height: 50,
  },
  textField: {
    marginBottom: '3%',
    width: '100%',
  },
  datePicker: {
    marginTop: '2%',
    // width: 600,
    height: 40,
  },
  button: {
    marginTop: '2%'
  },
  loading: {
    width: '100%',
  }
});

export default styles;